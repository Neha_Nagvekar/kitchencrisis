package com.gyro.kitchencrisis.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gyro.kitchencrisis.GameMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.foregroundFPS = 60;
		config.width = 600;
		config.height = 900;
		config.resizable = false;

		new LwjglApplication(new GameMain(), config);
	}
}
