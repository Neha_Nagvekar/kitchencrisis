package com.gyro.kitchencrisis;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Burger {

    private static final int DEFAULT_SPEEDY = 60;

    private Sprite burgerSprite;

    public float x;
    public float y;

    private int speedX;
    private int speedY;

    public boolean rebounded = false;

    public CollisionRect rect;
    private static final int COLLISION_OFFSET_X = 1;
    private static final int COLLISION_OFFSET_Y = 1;
    private static final int COLLISION_WIDTH = 14;
    private static final int COLLISION_HEIGHT = 13;

    private static final int REBOUND_MULTIPLIER = 5;

    public Burger(float chefX, float chefY, int speedX, int speedY) {
        burgerSprite = new Sprite(new Texture("Images/burger.png"));
        this.x = chefX+ burgerSprite.getWidth()/2;
        this.y = chefY;
        burgerSprite.setPosition(chefX, chefY);

        this.speedX = speedX;
        this.speedY = -speedY;

        rect = new CollisionRect(chefX+COLLISION_OFFSET_X, chefY+COLLISION_OFFSET_Y, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public Burger(float chefX, float chefY) {
        burgerSprite = new Sprite(new Texture("Images/burger.png"));
        this.x = chefX+ burgerSprite.getWidth()/2;
        this.y = chefY;
        burgerSprite.setPosition(chefX, chefY);

        this.speedX = 0;
        this.speedY = -DEFAULT_SPEEDY;

        rect = new CollisionRect(chefX+COLLISION_OFFSET_X, chefY+COLLISION_OFFSET_Y, COLLISION_WIDTH, COLLISION_HEIGHT);
    }


    public void update (float deltaTime) {
        x += speedX * deltaTime;
        y += speedY * deltaTime;
        burgerSprite.setPosition(x, y);
        rect.setPosition(x+COLLISION_OFFSET_X, y+COLLISION_OFFSET_Y);
    }

    public void reboundWall() {
        speedX = -speedX;
    }

    public void splatter() {

    }

    public void draw(SpriteBatch batch) {
        burgerSprite.draw(batch);
    }

    public void disposeTextures() {
        burgerSprite.getTexture().dispose();
    }


}
