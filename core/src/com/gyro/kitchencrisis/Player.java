package com.gyro.kitchencrisis;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Player {

    private static final int INIT_PLAYER_SPEED = 10;
    private static final int DELTA_PLAYER_SPEED = 100;

    private Sprite playerSprite;
    private float playerX;
    private float playerY;

    private int playerSpeed;

    private Texture shieldOn;
    private Texture shieldOff;
    public boolean shield;

    public CollisionRect shieldRect;
    private static final int SHIELD_COLLISION_OFFSET_X = 5;
    private static final int SHIELD_COLLISION_OFFSET_Y = 26;
    private static final int SHIELD_COLLISION_WIDTH = 23;
    private static final int SHIELD_COLLISION_HEIGHT = 25;

    public CollisionRect rect;
    private static final int COLLISION_OFFSET_X = 8;
    private static final int COLLISION_OFFSET_Y = 18;
    private static final int COLLISION_WIDTH = 17;
    private static final int COLLISION_HEIGHT = 25;

    private boolean lastTapLeft;

    private int gameWidth;

    public Player(int gameWidth) {
        shieldOn = new Texture("Images/player_shield.png");
        shieldOff = new Texture("Images/player.png");
        shield = false;
        playerSprite = new Sprite(shieldOff);
        playerSprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        lastTapLeft = false;
        this.gameWidth = gameWidth;
        playerX = gameWidth/2-playerSprite.getWidth()/2;
        playerY = 0-playerSprite.getHeight()/8;
        playerSprite.setPosition(playerX, playerY);
        playerSpeed = INIT_PLAYER_SPEED;

        rect = new CollisionRect(playerX + COLLISION_OFFSET_X, playerY + COLLISION_OFFSET_Y, COLLISION_WIDTH, COLLISION_HEIGHT);
        shieldRect = new CollisionRect(playerX + SHIELD_COLLISION_OFFSET_X, playerY + SHIELD_COLLISION_OFFSET_Y, SHIELD_COLLISION_WIDTH, SHIELD_COLLISION_HEIGHT);
    }

    public void draw(SpriteBatch batch) {
        playerSprite.draw(batch);
    }

    public void movePlayer(boolean tapLeft, float deltaTime) {
        playerSpeed += DELTA_PLAYER_SPEED*deltaTime;
        if (lastTapLeft!=tapLeft) {
            resetPlayerSpeed();
            lastTapLeft = tapLeft;
        }
        if (tapLeft) {
            playerX -= playerSpeed * Gdx.graphics.getDeltaTime();
            if (playerX < 0) {
                playerX = 0;
            }
        } else {
            playerX += playerSpeed * Gdx.graphics.getDeltaTime();
            if (playerX > gameWidth - playerSprite.getWidth()) {
                playerX = gameWidth - playerSprite.getWidth();
            }
        }
        playerSprite.setPosition(playerX, playerY);
        rect.setPosition(playerX+COLLISION_OFFSET_X, playerY+COLLISION_OFFSET_Y);
        shieldRect.setPosition(playerX+SHIELD_COLLISION_OFFSET_X, playerY+SHIELD_COLLISION_OFFSET_Y);
    }

    public void resetPlayerSpeed() {
        playerSpeed = INIT_PLAYER_SPEED;
    }

    public float getPlayerCenterX() {
        return playerX+playerSprite.getWidth()/2;
    }

    public void toggleShield() {
        if (shield) {
            playerSprite.setTexture(shieldOff);
        } else {
            playerSprite.setTexture(shieldOn);
        }
        shield = !shield;
    }

    public void disposeTextures() {
        shieldOn.dispose();
        shieldOff.dispose();
    }

}
