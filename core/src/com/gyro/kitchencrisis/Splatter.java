package com.gyro.kitchencrisis;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Splatter {

    private static final int BURGER_OFFSET_X = -31;
    private static final int BURGER_OFFSET_Y = 7;
    private static final int SHIELD_OFFSET_Y = -6;

    private static final int SPLATTER_FRAME_WIDTH = 64;
    private static final int SPLATTER_FRAME_HEIGHT = 32;
    private static final int SPLATTER_TILE_WIDTH = 2;
    private static final int SPLATTER_TILE_HEIGHT = 4;

    private TextureRegion[] splatterFrames;
    private Animation<TextureRegion> splatter;

    private float timeElapsed;
    private float x;
    private float y;


    public Splatter(float x, float y, boolean shield) {
        TextureRegion[][] tempSplatter = TextureRegion.split(new Texture("Images/splatter.png"), SPLATTER_FRAME_WIDTH, SPLATTER_FRAME_HEIGHT);
        splatterFrames = new TextureRegion[SPLATTER_TILE_WIDTH * SPLATTER_TILE_HEIGHT];
        for (int i = 0; i < SPLATTER_TILE_HEIGHT; i++) {
            for (int j = 0; j < SPLATTER_TILE_WIDTH; j++) {
                splatterFrames[i * SPLATTER_TILE_WIDTH + j] = tempSplatter[i][j];
            }
        }
        splatter = new Animation(1f / 12f, splatterFrames);

        this.x = x + BURGER_OFFSET_X;
        this.y = y + BURGER_OFFSET_Y;
        if (shield) {
            this.y += SHIELD_OFFSET_Y;
        }

        timeElapsed = 0;

    }

    public void update (float deltaTime) {
        timeElapsed += deltaTime;
    }

    public void draw (SpriteBatch batch) {
        batch.draw(splatter.getKeyFrame(timeElapsed, false), x, y);
    }


}
