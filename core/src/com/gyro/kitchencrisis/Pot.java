package com.gyro.kitchencrisis;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Pot {

    private static final int DEFAULT_SPEEDY = 60;

    private Sprite potSprite;
    private Music pot_deflect;

    public float x;
    public float y;

    private int speedX;
    private int speedY;

    public boolean deflected = false;
    public boolean hit_chef = false;



    public CollisionRect rect;
    private static final int COLLISION_OFFSET_X = 2;
    private static final int COLLISION_OFFSET_Y = 0;
    private static final int COLLISION_WIDTH = 13;
    private static final int COLLISION_HEIGHT = 10;

    private static final int REBOUND_MULTIPLIER = 5;

    public Pot(float chefX, float chefY, int speedX, int speedY) {
        potSprite = new Sprite(new Texture("pot.png"));
        this.x = chefX+potSprite.getWidth()/2;
        this.y = chefY;
        potSprite.setPosition(chefX, chefY);

        this.speedX = speedX;
        this.speedY = -speedY;

        rect = new CollisionRect(chefX+COLLISION_OFFSET_X, chefY+COLLISION_OFFSET_Y, COLLISION_WIDTH, COLLISION_HEIGHT);

        pot_deflect = Gdx.audio.newMusic(Gdx.files.internal("Sounds/pot_deflect.wav"));

        deflected = false;
    }

    public Pot(float chefX, float chefY) {
        potSprite = new Sprite(new Texture("Images/pot.png"));
        this.x = chefX+potSprite.getWidth()/2;
        this.y = chefY;
        potSprite.setPosition(chefX, chefY);

        this.speedX = 0;
        this.speedY = -DEFAULT_SPEEDY;

        rect = new CollisionRect(chefX+COLLISION_OFFSET_X, chefY+COLLISION_OFFSET_Y, COLLISION_WIDTH, COLLISION_HEIGHT);

        pot_deflect = Gdx.audio.newMusic(Gdx.files.internal("Sounds/pot_deflect.wav"));
    }


    public void update (float deltaTime) {
        x += speedX * deltaTime;
        y += speedY * deltaTime;
        potSprite.setPosition(x, y);
        rect.setPosition(x+COLLISION_OFFSET_X, y+COLLISION_OFFSET_Y);
    }

    public void reboundWall() {
        speedX = -speedX;
        pot_deflect.play();
    }

    public void reboundPlayer(float playerCenterX) {
        if (!deflected) {
            speedX = (int) (((x + COLLISION_WIDTH / 2) - playerCenterX) * REBOUND_MULTIPLIER);
            speedY = -speedY;
            pot_deflect.play();
            deflected = true;
        }
    }

    public void setXPosition(float x) {
        this.x = x;
        potSprite.setPosition(x, y);
    }


    public void draw(SpriteBatch batch) {
        potSprite.draw(batch);
    }

    public void dispose() {
        potSprite.getTexture().dispose();
        pot_deflect.dispose();
    }
}
