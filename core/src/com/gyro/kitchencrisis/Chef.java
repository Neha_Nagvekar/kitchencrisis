package com.gyro.kitchencrisis;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gyro.kitchencrisis.com.gyro.kitchencrisis.screens.MainGameScreen;

public class Chef {

    private static final float THROW_POT_PROBABILITY = 0.7f;
    private static final float PAUSE_TIME = 0.25f;
    private static final float MIN_CHEF_SPEED = 25;

    private Sprite chefSprite;
    private float chefX;
    private float chefY;

    private int chefSpeed;
    private float throwDelay;
    private float minThrowDelay;
    private float maxThrowDelay;
    private float targetPos;
    private float pauseTimer;
    boolean pause;

    private int gameWidth;
    private MainGameScreen screen;

    public CollisionRect rect;
    private static final int COLLISION_OFFSET_X = 7;
    private static final int COLLISION_OFFSET_Y = 14;
    private static final int COLLISION_WIDTH = 19;
    private static final int COLLISION_HEIGHT = 36;

    private Music throw_projectile;


    public Chef(MainGameScreen screen, int gameWidth, int gameHeight) {
        this.gameWidth = gameWidth;
        this.screen = screen;

        chefSprite = new Sprite(new Texture("Images/chef.png"));
        chefSprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        chefX = gameWidth/2-chefSprite.getWidth()/2;
        chefY = gameHeight-chefSprite.getHeight()*7/8;
        chefSprite.setPosition(chefX, chefY);

        chefSpeed = 50;
        throwDelay = 2.0f;
        minThrowDelay = 1.0f;
        maxThrowDelay = 2.0f;
        pauseTimer = PAUSE_TIME;
        pause = true;

        throw_projectile = Gdx.audio.newMusic(Gdx.files.internal("Sounds/chef_throw.wav"));

        rect = new CollisionRect(chefX+COLLISION_OFFSET_X, chefY+COLLISION_OFFSET_Y, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    //Chef moves to a random spot within a certain time (varies slightly) at a certain min speed, throws a projectile, pauses, repeats
    public void update(float deltaTime) {
        if (pause) {
            pauseTimer += deltaTime;
            if (pauseTimer>=PAUSE_TIME) {
                pause = false;
                pauseTimer = 0;

                //Use time to determine position and speed so that adjusting the games difficulty is easier
                throwDelay = (float)Math.random()*(maxThrowDelay-minThrowDelay)+minThrowDelay;
                float minDistance = throwDelay*MIN_CHEF_SPEED;
                if (chefX-minDistance<0) {
                    targetPos = (float)(Math.random()*(gameWidth - chefSprite.getWidth() - chefX - minDistance)  + chefX + minDistance);
                } else if (chefX+minDistance>gameWidth-chefSprite.getWidth()) {
                    targetPos = (float)(Math.random()*(chefX-minDistance));
                } else {
                    targetPos = (float)(Math.random()*(gameWidth-chefSprite.getWidth()-(minDistance*2)));
                    if (targetPos>chefX-minDistance&&targetPos<chefX+minDistance) {
                        targetPos += minDistance*2;
                    }
                }
                chefSpeed = (int)((targetPos-chefX)/throwDelay);
            }
        } else {
            chefX += chefSpeed * deltaTime;
            if ((chefSpeed>0&&chefX>targetPos)||(chefSpeed<0&&chefX<targetPos)) {
                chefX = targetPos;
                throwProjectile();
                pause = true;
            }
            chefSprite.setPosition(chefX, chefY);
            rect.setPosition(chefX+COLLISION_OFFSET_X, chefY+COLLISION_OFFSET_Y);
        }
    }

    public void throwProjectile() {
        throw_projectile.play();
        if (Math.random()<THROW_POT_PROBABILITY) {
            screen.pots.add(new Pot(chefX, chefY));
        } else {
            screen.burgers.add(new Burger(chefX, chefY));
        }
    }

    public void draw(SpriteBatch batch) {
        chefSprite.draw(batch);
    }

    public void dispose() {
        chefSprite.getTexture().dispose();
        throw_projectile.dispose();
    }

}
