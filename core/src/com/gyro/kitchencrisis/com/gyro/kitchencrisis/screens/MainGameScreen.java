package com.gyro.kitchencrisis.com.gyro.kitchencrisis.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.input.GestureDetector;
import com.gyro.kitchencrisis.Burger;
import com.gyro.kitchencrisis.Chef;
import com.gyro.kitchencrisis.GameGestureListener;
import com.gyro.kitchencrisis.GameMain;
import com.gyro.kitchencrisis.Player;
import com.gyro.kitchencrisis.Pot;
import com.gyro.kitchencrisis.Splatter;

import java.util.ArrayList;

public class MainGameScreen implements Screen {

    private static final int SCORE_CHEF_HIT = 100;
    private static final int SCORE_POT_DEFLECT = 20;
    private static final int SCORE_FOOD_CATCH = 5;


    private static final int ACTUAL_BG_HEIGHT = 320; //must be specified because of transparent pixels used to make texture dimensions a multiple of 2
    private static final int ACTUAL_BG_WIDTH = 240;

    private static final float TAP_DURATION = 0.2f;

    private static final int BORDER_PADDING = 5;

    private GameMain game;
    private Sprite game_bg;

    private Texture fade;
    private boolean isPaused;

    private boolean isGameOver;
    private Texture gameover;
    private static final int GAME_OVER_WIDTH = ACTUAL_BG_HEIGHT/16*9-20;
    private static final int GAME_OVER_HEIGHT = ACTUAL_BG_HEIGHT - 40;
    private static final int GAME_OVER_OFFSET_Y = 80;

    private Sprite button_replay;
    private boolean buttonReplayPressed;
    private static final int BUTTON_REPLAY_OFFSET_Y = -80;
    private static final int GAME_OVER_SCORE_OFFSET_Y = 5;
    private Music button;

    private Sprite button_pause;
    private boolean buttonPausePressed;

    private BitmapFont fontDark;
    private BitmapFont fontLight;
    private int score;
    private GlyphLayout fontLayout;

    private Player player;

    private Chef chef;

    public ArrayList<Pot> pots;
    public ArrayList<Burger> burgers;
    private ArrayList<Splatter> splatters;

    private GestureDetector.GestureListener gestureListener;
    private GestureDetector gestureDetector;

    private int health;
    private Texture heart;

    private float doubleTapPause;

    private OrthographicCamera camera;
    private int gameWidth;

    private Music pot_chefhit;
    private Music pot_hit;
    private Music food_fail;
    private Music food_catch;

    private Music game_loop;
    private Music game_over;
    private boolean gameOverPlayed;

    public MainGameScreen (GameMain game) {
        this.game = game;
    }


    @Override
    public void show() {
        gameWidth = ACTUAL_BG_HEIGHT*Gdx.graphics.getWidth()/Gdx.graphics.getHeight();  //handles different screen sizes

        fontLight = new BitmapFont(Gdx.files.internal("pixel_light.fnt"));
        fontDark = new BitmapFont(Gdx.files.internal("pixel_dark.fnt"));
        score = 0;
        fontLayout = new GlyphLayout();

        player = new Player(gameWidth);
        chef = new Chef(this, gameWidth, ACTUAL_BG_HEIGHT);

        pots = new ArrayList<Pot>();
        burgers = new ArrayList<Burger>();
        splatters = new ArrayList<Splatter>();

        gestureListener = new GameGestureListener(player);
        gestureDetector = new GestureDetector(20, TAP_DURATION, 1.1f, 0.15f, gestureListener); //numbers are default values
        Gdx.input.setInputProcessor(gestureDetector);
        doubleTapPause = 0;

        game_bg = new Sprite(new Texture("Images/background_game.png"));
        game_bg.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        game_bg.setPosition((gameWidth-game_bg.getWidth())/2, (ACTUAL_BG_HEIGHT-game_bg.getHeight())/2); //sets origin to edge of background shown

        fade = new Texture("Images/fade.png");
        isPaused = false;
        gameover = new Texture("Images/gameover.png");
        isGameOver = false;
        button_replay = new Sprite(new Texture("Images/button_replay.png"));
        button_replay.setPosition((gameWidth-button_replay.getWidth())/2, (ACTUAL_BG_HEIGHT-button_replay.getHeight())/2+BUTTON_REPLAY_OFFSET_Y);
        buttonReplayPressed = false;
        button = Gdx.audio.newMusic(Gdx.files.internal("Sounds/game_start.mp3"));
        button.setLooping(false);

        health = 3;
        heart = new Texture("Images/heart.png");

        pot_chefhit = Gdx.audio.newMusic(Gdx.files.internal("Sounds/pot_chefhit.wav"));
        pot_hit = Gdx.audio.newMusic(Gdx.files.internal("Sounds/pot_hit.wav"));
        food_fail = Gdx.audio.newMusic(Gdx.files.internal("Sounds/food_splat.mp3"));
        food_catch = Gdx.audio.newMusic(Gdx.files.internal("Sounds/food_catch.wav"));

        game_loop = Gdx.audio.newMusic(Gdx.files.internal("Sounds/game_loop.mp3"));
        game_loop.setLooping(true);
        game_loop.play();
        game_over = Gdx.audio.newMusic(Gdx.files.internal("Sounds/game_over.wav"));
        game_over.setLooping(false);
        gameOverPlayed = false;

        camera = new OrthographicCamera(gameWidth, ACTUAL_BG_HEIGHT);
        camera.translate(camera.viewportWidth/2, camera.viewportHeight/2);
        camera.update();
    }

    @Override
    public void render(float delta) {
        if (health<=0) {
            isPaused = true;
            isGameOver = true;
            if (!gameOverPlayed) {
                game_loop.stop();
                game_over.play();
                gameOverPlayed = true;
            }
        }

        //phone input
        if (!isPaused) {
            handeInput();
            chef.update(Gdx.graphics.getDeltaTime());
            updatePots();
            updateBurgers();
            updateSplatters();
        }


        //draw
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        game.batch.setProjectionMatrix(camera.combined);
        game_bg.draw(game.batch);
        
        for (int i=0; i<splatters.size(); i++) {
            splatters.get(i).draw(game.batch);
        }
        player.draw(game.batch);
        chef.draw(game.batch);
        for (int i=0; i<pots.size(); i++) {
            pots.get(i).draw(game.batch);
        }
        for (int i=0; i<burgers.size(); i++) {
            burgers.get(i).draw(game.batch);
        }
        drawHealth();
        
        if (!isGameOver) {
            drawScore();
        }
        if (isPaused) {
            game.batch.draw(fade, 0, 0, gameWidth, ACTUAL_BG_HEIGHT);
        }
        if (isGameOver) {
            if ( Gdx.input.justTouched() && isButtonPressed(button_replay, (int)button_replay.getWidth(), (int)button_replay.getHeight(), Gdx.input.getX(), Gdx.input.getY())) {
                button_replay.setTexture(new Texture("Images/button_replay_pressed.png"));
                buttonReplayPressed = true;
            }
            
            drawGameOver();
            
            if (!Gdx.input.isTouched()&& buttonReplayPressed) {
                button.play();
                if(button.getPosition() > 0.8f) {
                    this.dispose();
                    game.setScreen(new MainGameScreen(game));
                }
            }
        }
        game.batch.end();
    }


    private void handeInput() {
        if (Gdx.input.isTouched()) {
            doubleTapPause += Gdx.graphics.getDeltaTime();
            if (doubleTapPause > TAP_DURATION) {
                player.movePlayer(Gdx.input.getX() < Gdx.graphics.getWidth() / 2, Gdx.graphics.getDeltaTime());
            }
        } else {
            player.resetPlayerSpeed();
            doubleTapPause = 0;
        }

        //test input for desktop
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            player.toggleShield();
        }
    }

    private void updatePots() {
        for (int i = 0; i < pots.size(); i++) {
            pots.get(i).update(Gdx.graphics.getDeltaTime());

            //if pot leaves the screen vertically, remove
            if (pots.get(i).y + pots.get(i).rect.height < 0 || pots.get(i).y > ACTUAL_BG_HEIGHT) {
                pots.get(i).dispose();
                pots.remove(i);
                i--;
                continue;
            }
            //if pot hits edge of screen horizontally, bounce
            if (pots.get(i).x < 0 || pots.get(i).x + pots.get(i).rect.width > gameWidth) {
                pots.get(i).reboundWall();
                if (pots.get(i).x < 0) {
                    pots.get(i).setXPosition(0);
                } else {
                    pots.get(i).setXPosition(gameWidth - pots.get(i).rect.width);
                }
            }

            //if pot hits player, react based on whether shield is on or not
            if (player.shield && pots.get(i).rect.collidesWith(player.shieldRect)) {
                if (!pots.get(i).deflected) {
                    score += SCORE_POT_DEFLECT;
                }
                pots.get(i).reboundPlayer(player.getPlayerCenterX());
            } else if (pots.get(i).rect.collidesWith(player.rect)) {
                pots.get(i).dispose();
                pots.remove(i);
                pot_hit.play();
                health--;
                i--;
                continue;
            }

            //if pot hits chef, remove pot and add score bonus
            if (pots.get(i).rect.collidesWith(chef.rect)) {
                pots.get(i).dispose();
                pots.remove(i);
                pot_chefhit.play();
                score += SCORE_CHEF_HIT;
                i--;
                continue;
            }
        }
    }
    
    private void updateBurgers() {
        for (int i = 0; i < burgers.size(); i++) {
                burgers.get(i).update(Gdx.graphics.getDeltaTime());

                //if burger hits the bottom of the screen, splatter
                if (burgers.get(i).y + burgers.get(i).rect.height / 2 < 0) {
                    burgers.get(i).disposeTextures();
                    splatters.add(new Splatter(burgers.get(i).x, burgers.get(i).y, false));
                    food_fail.play();
                    burgers.remove(i);
                    health--;
                    i--;
                    continue;
                }
                //if burger hits edge of screen horizontally, bounce (currently does not happen, but might add later)
                if (burgers.get(i).x < 0 || burgers.get(i).x + burgers.get(i).rect.width > gameWidth) {
                    burgers.get(i).reboundWall();
                }

                //if burger hits player, react based on whether shield is on or not
                if (player.shield && burgers.get(i).rect.collidesWith(player.shieldRect)) {
                    burgers.get(i).disposeTextures();
                    splatters.add(new Splatter(burgers.get(i).x, burgers.get(i).y, true));
                    food_fail.play();
                    burgers.remove(i);
                    health--;
                    i--;
                    continue;
                } else if (burgers.get(i).rect.collidesWith(player.rect)) {
                    burgers.get(i).disposeTextures();
                    burgers.remove(i);
                    food_catch.play();
                    score += SCORE_FOOD_CATCH;
                    i--;
                    continue;
                }
            }
    }

    private void updateSplatters() {
            for (int i = 0; i < splatters.size(); i++) {
                splatters.get(i).update(Gdx.graphics.getDeltaTime());
            }
    }

    private void drawHealth() {
        for (int i=0; i<health; i++) {
            game.batch.draw(heart, BORDER_PADDING+heart.getWidth()*i, ACTUAL_BG_HEIGHT-heart.getHeight()-BORDER_PADDING);
        }
    }

    private void drawScore() {
        fontLayout.setText(fontLight, ""+score);
        fontDark.draw(game.batch, ""+score, gameWidth-fontLayout.width- BORDER_PADDING -1, ACTUAL_BG_HEIGHT- BORDER_PADDING -1);
        fontDark.draw(game.batch, ""+score, gameWidth-fontLayout.width- BORDER_PADDING -1, ACTUAL_BG_HEIGHT- BORDER_PADDING +1);
        fontDark.draw(game.batch, ""+score, gameWidth-fontLayout.width- BORDER_PADDING +1, ACTUAL_BG_HEIGHT- BORDER_PADDING -1);
        fontDark.draw(game.batch, ""+score, gameWidth-fontLayout.width- BORDER_PADDING +1, ACTUAL_BG_HEIGHT- BORDER_PADDING +1);
        fontLight.draw(game.batch, ""+score, gameWidth-fontLayout.width- BORDER_PADDING, ACTUAL_BG_HEIGHT- BORDER_PADDING);
    }

    private void drawGameOver() {
         game.batch.draw(fade, (gameWidth-GAME_OVER_WIDTH)/2, (ACTUAL_BG_HEIGHT-GAME_OVER_HEIGHT)/2, GAME_OVER_WIDTH, GAME_OVER_HEIGHT);
            game.batch.draw(fade, (gameWidth-GAME_OVER_WIDTH)/2, (ACTUAL_BG_HEIGHT-GAME_OVER_HEIGHT)/2, GAME_OVER_WIDTH, GAME_OVER_HEIGHT);
            game.batch.draw(gameover, (gameWidth-gameover.getWidth())/2, (ACTUAL_BG_HEIGHT-gameover.getHeight())/2+GAME_OVER_OFFSET_Y);

            fontLayout.setText(fontLight, ""+score);
            fontDark.getData().setScale(2);
            fontLight.getData().setScale(2);
            fontDark.draw(game.batch, ""+score, gameWidth/2-fontLayout.width/2-1, ACTUAL_BG_HEIGHT/2-1 + GAME_OVER_SCORE_OFFSET_Y);
            fontDark.draw(game.batch, ""+score, gameWidth/2-fontLayout.width/2-1, ACTUAL_BG_HEIGHT/2+1 + GAME_OVER_SCORE_OFFSET_Y);
            fontDark.draw(game.batch, ""+score, gameWidth/2-fontLayout.width/2+1, ACTUAL_BG_HEIGHT/2-1 + GAME_OVER_SCORE_OFFSET_Y);
            fontDark.draw(game.batch, ""+score, gameWidth/2-fontLayout.width/2+1, ACTUAL_BG_HEIGHT/2+1 + GAME_OVER_SCORE_OFFSET_Y);
            fontLight.draw(game.batch, ""+score, gameWidth/2-fontLayout.width/2, ACTUAL_BG_HEIGHT/2 + GAME_OVER_SCORE_OFFSET_Y);

            button_replay.draw(game.batch);
    }

    private boolean isButtonPressed(Sprite button, int actualWidth, int actualHeight, int inputX, int inputY) {
        inputX = inputX * gameWidth/Gdx.graphics.getWidth();
        inputY = (Gdx.graphics.getHeight() - inputY) * ACTUAL_BG_HEIGHT/Gdx.graphics.getHeight();

        int leftX = (int)(button.getX() + button.getWidth()/2 - actualWidth/2);
        int rightX = (int)(button.getX() + button.getWidth()/2 + actualWidth/2);
        int botY = (int)(button.getY() + button.getHeight()/2 - actualHeight/2);
        int topY = (int)(button.getY() + button.getHeight()/2 + actualHeight/2);

        return (inputX>leftX && inputX<rightX && inputY>botY && inputY<topY);
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        fade.dispose();
        gameover.dispose();
        game_bg.getTexture().dispose();
        button_replay.getTexture().dispose();
        player.disposeTextures();
        chef.dispose();
        heart.dispose();
        pot_hit.dispose();
        food_fail.dispose();
        food_catch.dispose();
        pot_chefhit.dispose();
        game_loop.dispose();
        game_over.dispose();
    }
}
