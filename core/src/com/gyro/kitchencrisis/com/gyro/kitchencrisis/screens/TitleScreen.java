package com.gyro.kitchencrisis.com.gyro.kitchencrisis.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.gyro.kitchencrisis.GameMain;

public class TitleScreen implements Screen {

    private static final int ACTUAL_BG_HEIGHT = 160; //must be specified because of transparent pixels used to make texture dimensions a multiple of 2
    private static final int ACTUAL_BG_WIDTH = 120;
    private static final int ACTUAL_BUTTON_HEIGHT = 24;
    private static final int ACTUAL_BUTTON_WIDTH = 46;



    private GameMain game;
    private Sprite menu_bg;
    private Sprite button_play;
    private OrthographicCamera camera;
    private int gameWidth;
    private boolean buttonPressed;

    private Music button;

    public TitleScreen(GameMain game) {
        this.game=game;
    }

    @Override
    public void show() {
        buttonPressed = false;
        gameWidth = ACTUAL_BG_HEIGHT*Gdx.graphics.getWidth()/Gdx.graphics.getHeight();  //handles different screen sizes

        menu_bg = new Sprite(new Texture("Images/background_title.png"));
        menu_bg.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        menu_bg.setPosition((gameWidth-menu_bg.getWidth())/2, (ACTUAL_BG_HEIGHT-menu_bg.getHeight())/2); //sets origin to edge of background shown

        button_play = new Sprite(new Texture("Images/button_play.png"));
        button_play.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        button_play.setPosition((gameWidth-button_play.getWidth())/2, 0);

        button = Gdx.audio.newMusic(Gdx.files.internal("Sounds/game_start.mp3"));
        button.setLooping(false);

        camera = new OrthographicCamera(gameWidth, ACTUAL_BG_HEIGHT);
        camera.translate(camera.viewportWidth/2, camera.viewportHeight/2);
        camera.update();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        game.batch.setProjectionMatrix(camera.combined);
        menu_bg.draw(game.batch);

        if ( Gdx.input.justTouched() && isButtonPressed(button_play, ACTUAL_BUTTON_WIDTH, ACTUAL_BUTTON_HEIGHT, Gdx.input.getX(), Gdx.input.getY())) {
            button_play.setTexture(new Texture("Images/button_play_pressed.png"));
            buttonPressed = true;
        }

        button_play.draw(game.batch);
        game.batch.end();

        if (!Gdx.input.isTouched()&&buttonPressed) {
            button.play();
            if(button.getPosition() > 0.8f) {
                this.dispose();
                game.setScreen(new MainGameScreen(game));
            }
        }
    }

    private boolean isButtonPressed(Sprite button, int actualWidth, int actualHeight, int inputX, int inputY) {
        inputX = inputX * gameWidth/Gdx.graphics.getWidth();
        inputY = (Gdx.graphics.getHeight() - inputY) * ACTUAL_BG_HEIGHT/Gdx.graphics.getHeight();

        int leftX = (int)(button.getX() + button.getWidth()/2 - actualWidth/2);
        int rightX = (int)(button.getX() + button.getWidth()/2 + actualWidth/2);
        int botY = (int)(button.getY() + button.getHeight()/2 - actualHeight/2);
        int topY = (int)(button.getY() + button.getHeight()/2 + actualHeight/2);

        return (inputX>leftX && inputX<rightX && inputY>botY && inputY<topY);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        menu_bg.getTexture().dispose();
        button_play.getTexture().dispose();
        button.dispose();
    }
}
