package com.gyro.kitchencrisis;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gyro.kitchencrisis.com.gyro.kitchencrisis.screens.TitleScreen;

public class GameMain extends Game {

	public SpriteBatch batch;
	
	@Override
	public void create () {
	    batch = new SpriteBatch();
	    this.setScreen(new TitleScreen(this));
	}

	@Override
	public void render () {
	    super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
