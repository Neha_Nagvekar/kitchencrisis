package com.gyro.kitchencrisis;

public class CollisionRect {
    public float x; //leftx
    public float y; //bottomy

    public float width;
    public float height;

    public CollisionRect(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public boolean collidesWith (CollisionRect rect) {
        return x+width>rect.x && x<rect.x+rect.width  && y+height>rect.y && y<rect.y+rect.height;
    }
}
